package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;	
		
		
		input = new Scanner(System.in);
		
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		input.nextLine();

		System.out.println("Introduce un numero como String");
		cadenaLeida = input.nextLine();

		
		if ( numeroLeido == Integer.parseInt(cadenaLeida)){
			System.out.println("Lo datos introducidos reprensentan el mismo numero");
//			
//			El error se produce porque tras leer el entero 'numeroLeido' (l�nea 20), el programa utiliza el 'nextLine()' (l�nea 23)
//			para limpiar el buffer en vez de leer un String. Al ocurrir esto, el 'if' (l�nea 26) trata de parsear una cadena vac�a a un entero
//			en su condici�n, lanzando una excepci�n puesto que el formato de 'cadenaLeida' no es el apropiado.
//
//			Se podr�a solucionar cambiando 'input.nextInt()' por 'Integer.parseInt(input.nextLine())'			
//			o bien a�adiendo un 'input.nextLine()' despu�s.
//			
		}
		
		input.close();
		
		
		
	}
}
