package ejercicios;

import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		char caracter=27;
//		No he sustituido el valor de 'caracter' porque no es necesario para el correcto funcionamiento del programa como he
//		explicado abajo.
		int posicionCaracter;
		String cadenaLeida, cadenaFinal;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un cadena");
		cadenaLeida = lector.nextLine();
		
		posicionCaracter = cadenaLeida.indexOf(caracter);
		
		if(posicionCaracter<0 || posicionCaracter>cadenaLeida.length()){
			System.out.println("La cadena no contiene el caracter "+caracter);
			System.exit(0);
		}
		
//		
//		El error sucede porque la variable 'caracter' esta igualada al n�mero 27 de la tabla ASCII, el cual no correspode a ninguna
//		letra, sino a [ESCAPE].
//
//		La soluci�n a este problema ser�a escribir un if para que el programa no de error en caso de que el caracter especificado no 
//		se encuentre dentro de la cadena introducida, para ello la condici�n deber�a ser que 'posicionCaracter' se saliese del tama�o
//		de 'cadenaLeida' y entonces parar el programa.
//		Tambi�n ser�a recomendable sustituir 'caracter' por un n�mero asociado a una letra en la tabla ASCII, puesto que una cadena
//		nunca va a contener [ESCAPE]. A pesar de ello seguir�a siendo necesario el if, ya que si ese caracter no estuviese dentro de la
//		cadena introducida seguir�a dando error.
//		
		cadenaFinal = cadenaLeida.substring(0, posicionCaracter);
		
		System.out.println(cadenaFinal);
		
		
		lector.close();
	}

}
