package ejercicios;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		for(int i = numeroLeido; i > 0 ; i--){
//			
//			Lo que est� ocurriendo es que el contador que esta dividiendo a 'numeroLeido' es mayor o igual que cero, por lo tanto
//			al llegar a la �ltimo iteraci�n del bucle, cuando i vale 0, este da una excepci�n aritm�tica.
//
//			La soluci�n es tan simple como sustituir la condici�n 'i >= 0' por 'i > 0' para que nunca se pueda producir una indeterminaci�n.
//			
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
		
		
		lector.close();
	}

}
