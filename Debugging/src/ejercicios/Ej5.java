//
//	Tras leer un n�mero por teclado, 'numeroLeido', el programa ejecuta un bucle 'for'(l�nea 38) con un contador que va desde 1 
//	(primer n�mero por el que se puede dividir cualquier cifra) hasta el propio n�mero introducido, con el objetivo de dividirlo
//	por 'numeroLeido'.
//	Cada vez que una de estas divisiones tenga como resto 0, implicar� que el programa ha encontrado un divisor de 'numeroLeido' y
//	sumar� una unidad al contador encargado de almacenar la cantidad total de divisores, 'cantidadDivisores'. Esta parte del c�digo
//	es llevada a cabo por el 'if' dentro del bucle (l�nea 39).
//	Acabado el bucle y obtenida la cantidad total de divisores de 'numeroLeido', el programa compara ese n�mero con la cantidad de
//	divisores que puede tener un n�mero primo, 2. Si 'cantidadDivisores' excede a dos, 'numeroLeido' claramente no ser� primo. En
//	caso de que 'cantidadDivisores' no sea mayor que 2, esto implicar� que efectivamente es un n�mero primo, ya que todo n�mero
//	tiene como m�nimo 2 divisores, no pudiendo se nunca menor que esa cantidad.
//	Esta �ltima parte es realizada por el if-else (l�nea 43).
//



package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qué realiza este programa
		 */
		
		
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		
		lector.close();
	}

}
