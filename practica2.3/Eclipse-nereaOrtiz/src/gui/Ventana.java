package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JMenuItem;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.JToggleButton;
import javax.swing.JTextArea;
import java.awt.Font;
import javax.swing.JPasswordField;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JToolBar;


public class Ventana extends JFrame {

	/**
	 * @author Nerea Ortiz
	 * @since 21/12/2017
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField txtMarta_1;
	private JTextField txtCamposPrez_1;
	private JTextField txtCRosas_1;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField txtl_1;
	private JTextField textField_3;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField txtMarta;
	private JTextField txtCamposPrez;
	private JTextField txtl;
	private JTextField textField_8;
	private JTextField txtCRosas;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField txtDrFernndezValds;
	private JTextField txtMaraPilarPre;
	private JPasswordField passwordField_1;
	private JTextField txtUsuario;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setFont(new Font("Beauty and the Beast", Font.PLAIN, 15));
		setBackground(new Color(255, 160, 122));
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana.class.getResource("/icons/teeth-icon-iloveimg-resized.png")));
		setTitle("Cl\u00EDnica Dental");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 626, 404);
		
		JMenuBar menu = new JMenuBar();
		setJMenuBar(menu);
		
		JMenu mnArchivo = new JMenu("Citas");
		menu.add(mnArchivo);

		JMenu mnNuevo = new JMenu("Nuevas...");
		mnArchivo.add(mnNuevo);
		
				JMenuItem mntmNewMenuItem = new JMenuItem("Primeras citas");
				mnNuevo.add(mntmNewMenuItem);
		
		JMenuItem mntmProyecto = new JMenuItem("Citas peri\u00F3dicas");
		mnNuevo.add(mntmProyecto);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Citas de urgencia");
		mnNuevo.add(mntmNewMenuItem_1);
		
		JMenuItem mntmVisualizar = new JMenuItem("Visualizar...");
		mnArchivo.add(mntmVisualizar);
		
		JMenuItem mntmBorrar = new JMenuItem("Borrar...");
		mnArchivo.add(mntmBorrar);
		
		JSeparator separator = new JSeparator();
		mnArchivo.add(separator);
		
		JMenu mnCalendario = new JMenu("Calendario");
		mnArchivo.add(mnCalendario);
		
		JMenuItem mntmFechasDisponibles = new JMenuItem("Fechas disponibles");
		mnCalendario.add(mntmFechasDisponibles);
		
		JMenuItem mntmCitasCanceladas = new JMenuItem("Citas canceladas");
		mnCalendario.add(mntmCitasCanceladas);
		
		JMenu mnPacientes = new JMenu("Pacientes");
		menu.add(mnPacientes);
		
		JMenuItem mntmBuscarPacientes = new JMenuItem("Buscar pacientes...");
		mnPacientes.add(mntmBuscarPacientes);
		
		JMenuItem mntmNuevoPaciente = new JMenuItem("Nuevo paciente...");
		mnPacientes.add(mntmNuevoPaciente);
		
		JMenuItem mntmDarDeBaja = new JMenuItem("Dar de baja...");
		mnPacientes.add(mntmDarDeBaja);
		
		JSeparator separator_4 = new JSeparator();
		mnPacientes.add(separator_4);
		
		JMenuItem mntmPolizas = new JMenuItem("Polizas");
		mnPacientes.add(mntmPolizas);
		
		JMenuItem mntmIntervencin = new JMenuItem("Intervenci\u00F3n");
		mnPacientes.add(mntmIntervencin);
		
		JMenu mnPersonal = new JMenu("Personal");
		menu.add(mnPersonal);
		
		JMenuItem mntmFichar = new JMenuItem("Fichar...");
		mnPersonal.add(mntmFichar);
		
		JSeparator separator_5 = new JSeparator();
		mnPersonal.add(separator_5);
		
		JMenuItem mntmDoctores = new JMenuItem("Doctores");
		mnPersonal.add(mntmDoctores);
		
		JMenuItem mntmHigienistas = new JMenuItem("Higienistas");
		mnPersonal.add(mntmHigienistas);
		
		JSeparator separator_6 = new JSeparator();
		mnPersonal.add(separator_6);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Claves administrativas");
		mnPersonal.add(mntmNewMenuItem_2);
		
		JMenu mnInstrumentalYProductos = new JMenu("Instrumental y productos");
		menu.add(mnInstrumentalYProductos);
		
		JMenuItem mntmExistencias = new JMenuItem("Existencias");
		mnInstrumentalYProductos.add(mntmExistencias);
		
		JMenuItem mntmPedidos = new JMenuItem("Pedidos");
		mnInstrumentalYProductos.add(mntmPedidos);
		
		JMenuItem mntmProveedores = new JMenuItem("Proveedores");
		mnInstrumentalYProductos.add(mntmProveedores);
		
		JMenuItem mntmFacturas = new JMenuItem("Facturas");
		mnInstrumentalYProductos.add(mntmFacturas);
		
		JToolBar toolBar = new JToolBar();
		menu.add(toolBar);
		
		JButton button_1 = new JButton("");
		button_1.setIcon(new ImageIcon(Ventana.class.getResource("/icons/Save_32.png")));
		toolBar.add(button_1);
		
		JButton button_2 = new JButton("");
		button_2.setIcon(new ImageIcon(Ventana.class.getResource("/icons/Copy-256-iloveimg-resized.png")));
		button_2.setSelectedIcon(null);
		toolBar.add(button_2);
		
		JButton button_3 = new JButton("");
		button_3.setIcon(new ImageIcon(Ventana.class.getResource("/icons/Delete_64-iloveimg-resized.png")));
		toolBar.add(button_3);
		
		JButton button_4 = new JButton("");
		button_4.setIcon(new ImageIcon(Ventana.class.getResource("/icons/Print_256-iloveimg-resized.png")));
		toolBar.add(button_4);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 127, 80));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBackground(new Color(255, 99, 71));
		tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		tabbedPane.setBounds(10, 77, 599, 242);
		contentPane.add(tabbedPane);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(176, 224, 230));
		tabbedPane.addTab("Datos", null, panel_2, null);
		panel_2.setLayout(null);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(Ventana.class.getResource("/icons/test2-iloveimg-resized.png")));
		label_1.setBounds(10, 11, 116, 180);
		panel_2.add(label_1);
		
		JLabel label_2 = new JLabel("N\u00BA Paciente");
		label_2.setBounds(136, 29, 71, 14);
		panel_2.add(label_2);
		
		textField_7 = new JTextField();
		textField_7.setBackground(new Color(176, 224, 230));
		textField_7.setHorizontalAlignment(SwingConstants.RIGHT);
		textField_7.setToolTipText("");
		textField_7.setText("001");
		textField_7.setEditable(false);
		textField_7.setColumns(10);
		textField_7.setBounds(215, 25, 37, 20);
		panel_2.add(textField_7);
		
		JLabel label_3 = new JLabel("Nombre");
		label_3.setBounds(136, 54, 71, 14);
		panel_2.add(label_3);
		
		txtMarta = new JTextField();
		txtMarta.setForeground(Color.DARK_GRAY);
		txtMarta.setBackground(new Color(176, 224, 230));
		txtMarta.setText("Marta");
		txtMarta.setEditable(false);
		txtMarta.setColumns(10);
		txtMarta.setBounds(215, 50, 120, 20);
		panel_2.add(txtMarta);
		
		JLabel label_4 = new JLabel("Apellidos");
		label_4.setBounds(136, 79, 71, 14);
		panel_2.add(label_4);
		
		txtCamposPrez = new JTextField();
		txtCamposPrez.setForeground(Color.DARK_GRAY);
		txtCamposPrez.setBackground(new Color(176, 224, 230));
		txtCamposPrez.setText("Campos P\u00E9rez");
		txtCamposPrez.setEditable(false);
		txtCamposPrez.setColumns(10);
		txtCamposPrez.setBounds(215, 75, 120, 20);
		panel_2.add(txtCamposPrez);
		
		JLabel label_5 = new JLabel("F. nacimiento");
		label_5.setBounds(136, 104, 94, 14);
		panel_2.add(label_5);
		
		JLabel label_6 = new JLabel("DNI");
		label_6.setBounds(136, 129, 71, 14);
		panel_2.add(label_6);
		
		JLabel label_7 = new JLabel("Sexo");
		label_7.setBounds(136, 154, 71, 14);
		panel_2.add(label_7);
		
		JRadioButton radioButton = new JRadioButton("Hombre");
		radioButton.setBackground(new Color(176, 224, 230));
		radioButton.setEnabled(false);
		radioButton.setBounds(215, 152, 71, 16);
		panel_2.add(radioButton);
		
		JRadioButton radioButton_1 = new JRadioButton("Mujer");
		radioButton_1.setBackground(new Color(176, 224, 230));
		radioButton_1.setSelected(true);
		radioButton_1.setBounds(288, 153, 61, 14);
		panel_2.add(radioButton_1);
		
		JLabel label_8 = new JLabel("Direcci\u00F3n");
		label_8.setBounds(370, 54, 71, 14);
		panel_2.add(label_8);
		
		txtl = new JTextField();
		txtl.setForeground(Color.DARK_GRAY);
		txtl.setBackground(new Color(176, 224, 230));
		txtl.setText("25963275L");
		txtl.setEditable(false);
		txtl.setColumns(10);
		txtl.setBounds(215, 125, 120, 20);
		panel_2.add(txtl);
		
		JLabel label_9 = new JLabel("Tel\u00E9fono 1");
		label_9.setBounds(370, 79, 71, 14);
		panel_2.add(label_9);
		
		JLabel label_10 = new JLabel("Tel\u00E9fono 2");
		label_10.setBounds(370, 104, 71, 14);
		panel_2.add(label_10);
		
		JLabel label_11 = new JLabel("Seguro");
		label_11.setBounds(370, 129, 71, 14);
		panel_2.add(label_11);
		
		JCheckBox checkBox = new JCheckBox("Adeslas Dental");
		checkBox.setBackground(new Color(176, 224, 230));
		checkBox.setEnabled(false);
		checkBox.setBounds(444, 125, 129, 23);
		panel_2.add(checkBox);
		
		JCheckBox checkBox_1 = new JCheckBox("Adeslas b\u00E1sico");
		checkBox_1.setBackground(new Color(176, 224, 230));
		checkBox_1.setEnabled(false);
		checkBox_1.setBounds(444, 150, 144, 23);
		panel_2.add(checkBox_1);
		
		textField_8 = new JTextField();
		textField_8.setForeground(Color.DARK_GRAY);
		textField_8.setBackground(new Color(176, 224, 230));
		textField_8.setText("25/11/1983");
		textField_8.setEditable(false);
		textField_8.setColumns(10);
		textField_8.setBounds(215, 100, 120, 20);
		panel_2.add(textField_8);
		
		txtCRosas = new JTextField();
		txtCRosas.setForeground(Color.DARK_GRAY);
		txtCRosas.setBackground(new Color(176, 224, 230));
		txtCRosas.setText("C/ Rosas, 9, 1\u00BAA");
		txtCRosas.setEditable(false);
		txtCRosas.setColumns(10);
		txtCRosas.setBounds(444, 51, 120, 20);
		panel_2.add(txtCRosas);
		
		textField_9 = new JTextField();
		textField_9.setForeground(Color.DARK_GRAY);
		textField_9.setBackground(new Color(176, 224, 230));
		textField_9.setText("976 420 596");
		textField_9.setEditable(false);
		textField_9.setColumns(10);
		textField_9.setBounds(444, 76, 120, 20);
		panel_2.add(textField_9);
		
		textField_10 = new JTextField();
		textField_10.setForeground(Color.DARK_GRAY);
		textField_10.setBackground(new Color(176, 224, 230));
		textField_10.setText("602 574 129");
		textField_10.setEditable(false);
		textField_10.setColumns(10);
		textField_10.setBounds(444, 101, 120, 20);
		panel_2.add(textField_10);
		
		JSeparator separator_2 = new JSeparator(SwingConstants.VERTICAL);
		separator_2.setBounds(355, 11, 22, 180);
		panel_2.add(separator_2);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(176, 224, 230));
		tabbedPane.addTab("Actualizar", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblNPaciente = new JLabel("N\u00BA Paciente");
		lblNPaciente.setBounds(10, 11, 71, 14);
		panel.add(lblNPaciente);
		
		textField = new JTextField();
		textField.setBackground(new Color(176, 224, 230));
		textField.setHorizontalAlignment(SwingConstants.RIGHT);
		textField.setToolTipText("");
		textField.setText("001");
		textField.setEditable(false);
		textField.setBounds(102, 8, 37, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 36, 71, 14);
		panel.add(lblNombre);
		
		txtMarta_1 = new JTextField();
		txtMarta_1.setText("Marta");
		txtMarta_1.setColumns(10);
		txtMarta_1.setBounds(102, 33, 173, 20);
		panel.add(txtMarta_1);
		
		JLabel lblApellidos = new JLabel("Apellidos");
		lblApellidos.setBounds(10, 61, 71, 14);
		panel.add(lblApellidos);
		
		txtCamposPrez_1 = new JTextField();
		txtCamposPrez_1.setText("Campos P\u00E9rez");
		txtCamposPrez_1.setColumns(10);
		txtCamposPrez_1.setBounds(102, 58, 173, 20);
		panel.add(txtCamposPrez_1);
		
		JLabel lblDni = new JLabel("F. nacimiento");
		lblDni.setBounds(10, 86, 94, 14);
		panel.add(lblDni);
		
		JLabel lblDireccin = new JLabel("Direcci\u00F3n");
		lblDireccin.setBounds(334, 36, 71, 14);
		panel.add(lblDireccin);
		
		txtCRosas_1 = new JTextField();
		txtCRosas_1.setText("C/ Rosas, 9, 1\u00BAA");
		txtCRosas_1.setColumns(10);
		txtCRosas_1.setBounds(408, 33, 173, 20);
		panel.add(txtCRosas_1);
		
		JSeparator separator_1 = new JSeparator(SwingConstants.VERTICAL);
		separator_1.setBounds(302, 11, 22, 144);
		panel.add(separator_1);
		
		JLabel lblSexo = new JLabel("Sexo");
		lblSexo.setBounds(10, 136, 71, 14);
		panel.add(lblSexo);
		
		JRadioButton rdbtnHombre = new JRadioButton("Hombre");
		rdbtnHombre.setBackground(new Color(176, 224, 230));
		buttonGroup.add(rdbtnHombre);
		rdbtnHombre.setBounds(102, 135, 71, 16);
		panel.add(rdbtnHombre);
		
		JRadioButton rdbtnMujer = new JRadioButton("Mujer");
		rdbtnMujer.setBackground(new Color(176, 224, 230));
		rdbtnMujer.setSelected(true);
		buttonGroup.add(rdbtnMujer);
		rdbtnMujer.setBounds(175, 136, 71, 14);
		panel.add(rdbtnMujer);
		
		JLabel label = new JLabel("DNI");
		label.setBounds(10, 111, 71, 14);
		panel.add(label);
		
		txtl_1 = new JTextField();
		txtl_1.setText("25963275L");
		txtl_1.setColumns(10);
		txtl_1.setBounds(102, 108, 173, 20);
		panel.add(txtl_1);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBackground(new Color(224, 255, 255));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"}));
		comboBox.setSelectedIndex(24);
		comboBox.setBounds(102, 83, 48, 20);
		panel.add(comboBox);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBackground(new Color(224, 255, 255));
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"}));
		comboBox_1.setSelectedIndex(10);
		comboBox_1.setBounds(155, 83, 48, 20);
		panel.add(comboBox_1);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setBackground(new Color(224, 255, 255));
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"1980", "1981", "1982", "1983", "1994", "1985", "1986", "1987", "1988", "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018"}));
		comboBox_2.setSelectedIndex(3);
		comboBox_2.setBounds(209, 83, 66, 20);
		panel.add(comboBox_2);
		
		JLabel lblTelfono = new JLabel("Tel\u00E9fono 1");
		lblTelfono.setBounds(334, 61, 71, 14);
		panel.add(lblTelfono);
		
		textField_3 = new JTextField();
		textField_3.setText("976 420 596");
		textField_3.setColumns(10);
		textField_3.setBounds(408, 58, 173, 20);
		panel.add(textField_3);
		
		textField_6 = new JTextField();
		textField_6.setText("602 574 129");
		textField_6.setColumns(10);
		textField_6.setBounds(408, 83, 173, 20);
		panel.add(textField_6);
		
		JLabel lblTelfono_1 = new JLabel("Tel\u00E9fono 2");
		lblTelfono_1.setBounds(334, 86, 71, 14);
		panel.add(lblTelfono_1);
		
		JLabel lblSeguro = new JLabel("Seguro");
		lblSeguro.setBounds(334, 111, 71, 14);
		panel.add(lblSeguro);
		
		JCheckBox chckbxAdeslasDental = new JCheckBox("Adeslas Dental");
		chckbxAdeslasDental.setBackground(new Color(176, 224, 230));
		chckbxAdeslasDental.setBounds(408, 107, 129, 23);
		panel.add(chckbxAdeslasDental);
		
		JCheckBox chckbxAdeslasntegro = new JCheckBox("Adeslas b\u00E1sico");
		chckbxAdeslasntegro.setBackground(new Color(176, 224, 230));
		chckbxAdeslasntegro.setBounds(408, 132, 144, 23);
		panel.add(chckbxAdeslasntegro);
		
		JButton btnNewButton = new JButton("Guardar");
		btnNewButton.setBackground(Color.DARK_GRAY);
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setIcon(new ImageIcon(Ventana.class.getResource("/icons/Save_32-iloveimg-resized.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBounds(241, 180, 129, 23);
		panel.add(btnNewButton);
		
		JPanel panel_1 = new JPanel();
		panel_1.setForeground(Color.DARK_GRAY);
		panel_1.setBackground(new Color(176, 224, 230));
		tabbedPane.addTab("Historial", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblOdontlogo = new JLabel("Odont\u00F3logo");
		lblOdontlogo.setBounds(10, 11, 71, 14);
		panel_1.add(lblOdontlogo);
		
		txtDrFernndezValds = new JTextField();
		txtDrFernndezValds.setForeground(Color.DARK_GRAY);
		txtDrFernndezValds.setBackground(new Color(176, 224, 230));
		txtDrFernndezValds.setText("Dr. Fern\u00E1ndez Vald\u00E9s");
		txtDrFernndezValds.setEditable(false);
		txtDrFernndezValds.setColumns(10);
		txtDrFernndezValds.setBounds(91, 8, 144, 20);
		panel_1.add(txtDrFernndezValds);
		
		JLabel lblHigienista = new JLabel("Higienista");
		lblHigienista.setBounds(10, 36, 71, 14);
		panel_1.add(lblHigienista);
		
		txtMaraPilarPre = new JTextField();
		txtMaraPilarPre.setForeground(Color.DARK_GRAY);
		txtMaraPilarPre.setBackground(new Color(176, 224, 230));
		txtMaraPilarPre.setText("Mar\u00EDa Pilar P\u00E9re Noe");
		txtMaraPilarPre.setEditable(false);
		txtMaraPilarPre.setColumns(10);
		txtMaraPilarPre.setBounds(91, 33, 144, 20);
		panel_1.add(txtMaraPilarPre);
		
		JLabel lblDiagnsticoClnico = new JLabel("Diagn\u00F3stico");
		lblDiagnsticoClnico.setBounds(10, 61, 71, 14);
		panel_1.add(lblDiagnsticoClnico);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(91, 57, 392, 70);
		panel_1.add(scrollPane);
		
		JTextArea txtrLaPacienteProgresa = new JTextArea();
		txtrLaPacienteProgresa.setFont(new Font("Tahoma", Font.PLAIN, 11));
		txtrLaPacienteProgresa.setText("-- Anterior consulta\r\nLa paciente progresa favorablemente y se recupera m\u00E1s r\u00E1pido de lo esperado \r\nde la intervenci\u00F3n.");
		scrollPane.setViewportView(txtrLaPacienteProgresa);
		
		JLabel lblTratamiento = new JLabel("Tratamiento");
		lblTratamiento.setBounds(10, 135, 71, 14);
		panel_1.add(lblTratamiento);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(91, 131, 392, 70);
		panel_1.add(scrollPane_1);
		
		JTextArea txtrTratamiento = new JTextArea();
		txtrTratamiento.setForeground(Color.BLACK);
		txtrTratamiento.setFont(new Font("Tahoma", Font.PLAIN, 11));
		txtrTratamiento.setText("Tratamiento:");
		txtrTratamiento.setWrapStyleWord(true);
		scrollPane_1.setViewportView(txtrTratamiento);
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.setBackground(Color.DARK_GRAY);
		btnNewButton_1.setIcon(new ImageIcon(Ventana.class.getResource("/icons/Save_64.png")));
		btnNewButton_1.setBounds(493, 87, 89, 70);
		panel_1.add(btnNewButton_1);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(176, 224, 230));
		tabbedPane.addTab("Datos bancarios", null, panel_3, null);
		panel_3.setLayout(null);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setForeground(Color.BLACK);
		passwordField_1.setToolTipText("");
		passwordField_1.setBounds(241, 113, 173, 20);
		panel_3.add(passwordField_1);
		
		txtUsuario = new JTextField();
		txtUsuario.setForeground(Color.BLACK);
		txtUsuario.setColumns(10);
		txtUsuario.setBounds(241, 84, 173, 20);
		panel_3.add(txtUsuario);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUsuario.setForeground(new Color(255, 255, 255));
		lblUsuario.setBounds(160, 87, 71, 14);
		panel_3.add(lblUsuario);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a");
		lblContrasea.setHorizontalAlignment(SwingConstants.RIGHT);
		lblContrasea.setForeground(new Color(255, 255, 255));
		lblContrasea.setBounds(160, 116, 71, 14);
		panel_3.add(lblContrasea);
		
		JToggleButton tglbtnGuardarCambios = new JToggleButton("Consultar");
		tglbtnGuardarCambios.setForeground(Color.WHITE);
		tglbtnGuardarCambios.setBackground(Color.DARK_GRAY);
		tglbtnGuardarCambios.setBounds(241, 144, 116, 23);
		panel_3.add(tglbtnGuardarCambios);
		tglbtnGuardarCambios.setIcon(new ImageIcon(Ventana.class.getResource("/icons/Blue-Arrow-Up-32-iloveimg-resized.png")));
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Ventana.class.getResource("/icons/Buzz_Private-0-iloveimg-resized(3).png")));
		lblNewLabel.setBounds(146, 0, 314, 214);
		panel_3.add(lblNewLabel);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(new Color(176, 224, 230));
		tabbedPane.addTab("Citas", null, panel_4, null);
		panel_4.setLayout(null);
		
		JLabel lblFecha = new JLabel("Fecha");
		lblFecha.setIcon(new ImageIcon(Ventana.class.getResource("/icons/Calendar-256-iloveimg-resized.png")));
		lblFecha.setBounds(28, 14, 66, 14);
		panel_4.add(lblFecha);
		
		JComboBox comboBox_3 = new JComboBox();
		comboBox_3.setModel(new DefaultComboBoxModel(new String[] {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"}));
		comboBox_3.setSelectedIndex(0);
		comboBox_3.setBackground(new Color(224, 255, 255));
		comboBox_3.setBounds(104, 11, 48, 20);
		panel_4.add(comboBox_3);
		
		JComboBox comboBox_4 = new JComboBox();
		comboBox_4.setModel(new DefaultComboBoxModel(new String[] {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"}));
		comboBox_4.setSelectedIndex(0);
		comboBox_4.setBackground(new Color(224, 255, 255));
		comboBox_4.setBounds(157, 11, 48, 20);
		panel_4.add(comboBox_4);
		
		JComboBox comboBox_5 = new JComboBox();
		comboBox_5.setModel(new DefaultComboBoxModel(new String[] {"2018", "2019", "2020", "2021"}));
		comboBox_5.setSelectedIndex(0);
		comboBox_5.setBackground(new Color(224, 255, 255));
		comboBox_5.setBounds(209, 11, 66, 20);
		panel_4.add(comboBox_5);
		
		JLabel lblPrioridad = new JLabel("Prioridad");
		lblPrioridad.setBounds(23, 62, 71, 14);
		panel_4.add(lblPrioridad);
		
		JSlider slider = new JSlider();
		slider.setValue(5);
		slider.setMinimum(1);
		slider.setMaximum(10);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setPaintTicks(true);
		slider.setBackground(new Color(176, 224, 230));
		slider.setBounds(89, 48, 200, 45);
		panel_4.add(slider);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setBounds(20, 39, 545, 15);
		panel_4.add(separator_3);
		
		JLabel lblHora = new JLabel("Hora");
		lblHora.setIcon(new ImageIcon(Ventana.class.getResource("/icons/Clock_64-iloveimg-resized.png")));
		lblHora.setBounds(355, 14, 66, 14);
		panel_4.add(lblHora);
		
		JSpinner spinner = new JSpinner();
		spinner.setBackground(new Color(224, 255, 255));
		spinner.setModel(new SpinnerNumberModel(8, 8, 20, 1));
		spinner.setBounds(413, 11, 39, 20);
		panel_4.add(spinner);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setBackground(new Color(224, 255, 255));
		spinner_1.setModel(new SpinnerNumberModel(0, 0, 59, 1));
		spinner_1.setBounds(462, 11, 39, 20);
		panel_4.add(spinner_1);
		
		JLabel label_12 = new JLabel(":");
		label_12.setBounds(454, 14, 11, 14);
		panel_4.add(label_12);
		
		JLabel lblMotivo = new JLabel("Motivo");
		lblMotivo.setBounds(23, 127, 71, 14);
		panel_4.add(lblMotivo);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(89, 113, 200, 45);
		panel_4.add(scrollPane_2);
		
		JTextArea textArea = new JTextArea();
		textArea.setFont(new Font("Tahoma", Font.PLAIN, 11));
		scrollPane_2.setViewportView(textArea);
		
		JLabel lblAsistencia = new JLabel("Asistencia");
		lblAsistencia.setBounds(355, 127, 71, 14);
		panel_4.add(lblAsistencia);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("");
		chckbxNewCheckBox.setBackground(new Color(176, 224, 230));
		chckbxNewCheckBox.setBounds(421, 121, 27, 23);
		panel_4.add(chckbxNewCheckBox);
		
		JSpinner spinner_2 = new JSpinner();
		spinner_2.setModel(new SpinnerNumberModel(1, 1, 15, 1));
		spinner_2.setBounds(421, 59, 39, 20);
		panel_4.add(spinner_2);
		
		JLabel lblGabinete = new JLabel("Gabinete");
		lblGabinete.setBounds(355, 62, 71, 14);
		panel_4.add(lblGabinete);
		
		JButton button = new JButton("Guardar");
		button.setBackground(Color.DARK_GRAY);
		button.setForeground(new Color(255, 255, 255));
		button.setIcon(new ImageIcon(Ventana.class.getResource("/icons/Save_32-iloveimg-resized.png")));
		button.setBounds(231, 180, 129, 23);
		panel_4.add(button);
		
		textField_1 = new JTextField();
		textField_1.setBounds(155, 30, 434, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Buscar paciente");
		lblNewLabel_1.setFont(new Font("Beauty and the Beast", Font.PLAIN, 11));
		lblNewLabel_1.setIcon(new ImageIcon(Ventana.class.getResource("/icons/Glass-64-iloveimg-resized.png")));
		lblNewLabel_1.setBounds(30, 33, 115, 14);
		contentPane.add(lblNewLabel_1);
	}
}
