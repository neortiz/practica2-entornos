import java.util.Scanner;
public class Main {
	static Scanner in = new Scanner(System.in);
	public static void main(String[] args) {
		
		int[] vector;
		int j = (int) (Math.random()*(15-1)+1);
		vector= new int [j];
		for(int k = 0; k<vector.length; k++){
			vector[k]=(int) (Math.random()*30);
		}
		
		int menu=0;
		
		while(menu>=0){
			System.out.println("\n\t\t\tMENU");
			System.out.println("\t\t\t____");
			System.out.println("\n1.\tVisualizar el vector.");
			System.out.println("2.\tModificar valores del vector.");
			System.out.println("3.\tVisualizar valores pares.");
			System.out.println("4.\tVisualizar valores impares.");
			System.out.println("5.\tVisualizar la suma de los divisibles para n.");
			System.out.println("6.\tVisualizar la desviaci�n respecto de la media.");
			System.out.println("7.\tVisualizar posiciones pares.");
			System.out.println("8.\tVisualizar posiciones impares.");
			System.out.println("9.\tVisualizar los valores en orden inverso.");
			System.out.println("10.\tGenerar un nuevo vector.");
			System.out.println("11.\tIntroducir un nuevo vector.");			
			System.out.println("12.\tSalir.");
			
			System.out.print("\t\tElegir opci�n:_");
			menu = Integer.parseInt(in.nextLine());
			
			switch(menu){
				case 1: 	visualizeVector(vector);
							break;
				case 2:		modifyVector(vector);
							break;
				case 3: 	evenValues(vector);
							break;
				case 4: 	oddValues(vector);
							break;
				case 5:		divisibleForN(vector);
							break;
				case 6: 	averageDeviation(vector);
							break;
				case 7: 	evenPositions(vector);
							break;
				case 8: 	oddPositions(vector);
							break;
				case 9: 	reverseOrder(vector);
							break;
				case 10:	vector=generateNewVector(vector);
							break;
				case 11:	vector=introduceNewVector(vector);
							break;
				case 12: 	System.exit(0);
			}
		}
		in.close();
	}
	static void visualizeVector(int[] vector){
		System.out.println("\n\n\t\t\tMENU");
		System.out.println("\t\t\t____");
		System.out.println("\n1.\tVisualizar el vector.");
		for  (int x=0; x<vector.length; x++) System.out.print("\t"+vector[x]+"   ");
		System.out.println("");
	}
	static void modifyVector(int[] vector){		
		String will="si";
		System.out.println("\n\n\t\t\tMENU");
		System.out.println("\t\t\t____");
		System.out.println("\n2.\tModificar valores del vector.");
		while(will.equalsIgnoreCase("si")){
			System.out.print("\t\tSeleccione la posici�n que quieres cambiar:_");
			int i = Integer.parseInt(in.nextLine());
			while (i < 0 || i > vector.length-1){
                System.out.println("\t\t\tError.");
				System.out.print("\t\tIntroduzca la posici�n del valor que desea cambiar:_");
                i = Integer.parseInt(in.nextLine());
            }
			System.out.print("\t\tIntroduzca el nuevo valor:_");
			vector[i]=Integer.parseInt(in.nextLine());
			System.out.print("\t�Desea cambiar alg�n valor m�s? (Si/No)_");
			will=in.nextLine();
			while(!will.equalsIgnoreCase("si")&&!will.equalsIgnoreCase("no")){
				System.out.print("\t\tError. �Desea cambiar alg�n valor m�s? (Si/No)_");
				will=in.nextLine();
			}
		}
	}
	static void evenValues(int[] vector){
		System.out.println("\n\n\t\t\tMENU");
		System.out.println("\t\t\t____");
		System.out.println("\n3.\tVisualizar valores pares.\n");
		for  (int i=0; i<vector.length; i++) if(vector[i]%2==0) System.out.print("\t"+vector[i]+"   ");
		System.out.println("");
	}
	static void oddValues(int[] vector){
		System.out.println("\n\n\t\t\tMENU");
		System.out.println("\t\t\t____");
		System.out.println("\n4.\tVisualizar valores impares.\n");
		for  (int i=0; i<vector.length; i++) if(vector[i]%2==1) System.out.print("\t"+vector[i]+"   ");
		System.out.println("");
	}
	static void divisibleForN(int[] vector){
		String will="si";
		System.out.println("\n\n\t\t\tMENU");
		System.out.println("\t\t\t____");
		System.out.println("\n5.\tVisualizar la suma de los divisibles para n.");
		while(will.equalsIgnoreCase("si")){
			int a = 0;
			System.out.print("\t\tIntroduzca un n�mero n para continuar:_");
			int n = Integer.parseInt(in.nextLine());
			for  (int i=0; i<vector.length; i++) if(vector[i]%n==0) a+=vector[i];
			System.out.println("\t\tLa suma de todos los n�meros divisibles para "+n+" es "+a);
			System.out.print("\t�Desea calcular alguna suma m�s? (Si/No)_");
			will=in.nextLine();
			while(!will.equalsIgnoreCase("si")&&!will.equalsIgnoreCase("no")){
				System.out.print("\t\tError. �Desea calcular alguna suma m�s? (Si/No)_");
				will=in.nextLine();
			}
		}
		System.out.println("");
	}
	static void averageDeviation(int[] vector){
		float m, d, a=0;
		System.out.println("\n\n\t\t\tMENU");
		System.out.println("\t\t\t____");
		System.out.println("\n6.\tVisualizar la desviaci�n respecto de la media.");
		for  (int x=0; x<vector.length; x++) a+=vector[x];
		m=a/vector.length;
		System.out.println("\n\t\tLa suma es "+a+" media es "+m);
		for  (int i=0; i<vector.length; i++){
			d=Math.abs(vector[i]-m);
			System.out.println("\n\t\t\ti = "+(i+1)+"\tx = "+vector[i]+"\td = "+d);
		}
		System.out.println("");
	}
	static void evenPositions(int[] vector){
		System.out.println("\n\n\t\t\tMENU");
		System.out.println("\t\t\t____");
		System.out.println("\n7.\tVisualizar posiciones pares.");
		for  (int i=0; i<vector.length; i++) if(i%2==0) System.out.print("\t"+vector[i]+"   ");
	}
	static void oddPositions(int[] vector){
		System.out.println("\n\n\t\t\tMENU");
		System.out.println("\t\t\t____");
		System.out.println("\n8.\tVisualizar posiciones impares.");
		for  (int i=0; i<vector.length; i++) if(i%2==1) System.out.print("\t"+vector[i]+"   ");
	}
	static void reverseOrder(int[] vector){
		System.out.println("\n\n\t\t\tMENU");
		System.out.println("\t\t\t____");
		System.out.println("\n9.\tVisualizar los valores en orden inverso.");
		for  (int i=vector.length-1; i>=0; i--) System.out.print("\t"+vector[i]+"   ");
	}
	static int[] generateNewVector(int[] vector){
		int j;
		System.out.println("\n\n\t\t\tMENU");
		System.out.println("\t\t\t____");
		System.out.println("\n10.\tGenerar un nuevo vector.");
		System.out.print("\t\t�Desea concretar la dimensi�n del nuevo vector? (Si/No)_");
		String will = in.nextLine();
		while(!will.equalsIgnoreCase("si")&&!will.equalsIgnoreCase("no")){
			System.out.print("\t\t\tError. �Desea concretar la dimensi�n del nuevo vector? (Si/No)_");
			will=in.nextLine();
		}
		if(will.equalsIgnoreCase("si")){
			System.out.print("\t\tIntroduca la dimensi�n del nuevo vector:_");
			j = Integer.parseInt(in.nextLine());
		}
		else j = (int) (Math.random()*(15-1)+1);
		vector= new int [j];
		for(int k = 0; k<vector.length; k++) vector[k]=(int) (Math.random()*30);
		for  (int x=0; x<vector.length; x++) System.out.print("\t"+vector[x]+"   ");
		return(vector);
	}
	static int[] introduceNewVector(int[] vector){
		String will;
		System.out.println("\n\n\t\t\tMENU");
		System.out.println("\t\t\t____");
		System.out.println("\n11.\tIntroducir un nuevo vector.");
		System.out.print("\t\tIntroduca la dimensi�n del nuevo vector:_");
		int j = Integer.parseInt(in.nextLine()); vector= new int [j];
		for  (int x=0; x<j; x++){
			System.out.print("\t\tIntroduzca un n�mero en la posici�n "+x+" del nuevo vector:_");
			int n=Integer.parseInt(in.nextLine()); vector[x]=n;
		}
		do{
			System.out.print("\t\tEl nuevo vector es:");
            for (int x = 0; x < j; x++) System.out.println("\t" + vector[x] + "   ");
			System.out.print("\n\t\t�El nuevo vector es correcto? (Si/No)_");
			will = in.nextLine();
			while(!will.equalsIgnoreCase("si")&&!will.equalsIgnoreCase("no")){
				System.out.print("\t\t\tError. �El nuevo vector es correcto? (Si/No)_");
				will=in.nextLine();
			}
			if(will.equalsIgnoreCase("no")){
				String confirm = "si"; do{
					System.out.print("\t\t\tIntroduzca la posici�n del valor que desea cambiar:_");
					int p = Integer.parseInt(in.nextLine());
					while (p < 0 || p > j-1){
	                    System.out.println("\t\t\t\tError.");
	    				System.out.print("\t\t\tIntroduzca la posici�n del valor que desea cambiar:_");
	                    p = Integer.parseInt(in.nextLine());
	                }
					System.out.print("\t\t\tIntroduzca un nuevo valor para la posici�n "+p+":_");
					int v = Integer.parseInt(in.nextLine()); vector[p] = v;
					System.out.print("\t\t\t�Desea cambiar alg�n valor m�s? (Si/No)_");
					confirm = in.nextLine();
					while(!confirm.equalsIgnoreCase("si")&&!confirm.equalsIgnoreCase("no")){
						System.out.print("\t\t\tError. �Desea cambiar alg�n valor m�s? (Si/No)_");
						confirm=in.nextLine();
					}
				}while(confirm.equalsIgnoreCase("si"));
			}
		}while(will.equalsIgnoreCase("no"));
		return(vector);
	}
}