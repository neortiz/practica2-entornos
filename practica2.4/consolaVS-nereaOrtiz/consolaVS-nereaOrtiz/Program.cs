﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int[] vector;
            int j = (int)(rnd.Next(1, 15));
            vector = new int[j];
            for (int k = 0; k < vector.Length; k++)
            {
                vector[k] = (int)(rnd.Next(0, 30));
            }

            int menu = 0;

            while (menu >= 0)
            {
                Console.WriteLine("\n\t\t\tMENU");
                Console.WriteLine("\t\t\t____");
                Console.WriteLine("\n1.\tVisualizar el vector.");
                Console.WriteLine("2.\tModificar valores del vector.");
                Console.WriteLine("3.\tVisualizar valores pares.");
                Console.WriteLine("4.\tVisualizar valores impares.");
                Console.WriteLine("5.\tVisualizar la suma de los divisibles para n.");
                Console.WriteLine("6.\tVisualizar la desviación respecto de la media.");
                Console.WriteLine("7.\tVisualizar posiciones pares.");
                Console.WriteLine("8.\tVisualizar posiciones impares.");
                Console.WriteLine("9.\tVisualizar los valores en orden inverso.");
                Console.WriteLine("10.\tGenerar un nuevo vector.");
                Console.WriteLine("11.\tIntroducir un nuevo vector.");
                Console.WriteLine("12.\tSalir.");

                Console.Write("\t\tElegir opción:_");
                menu = int.Parse(Console.ReadLine());

                switch (menu)
                {
                    case 1:
                        visualizeVector(vector);
                        break;
                    case 2:
                        modifyVector(vector);
                        break;
                    case 3:
                        evenValues(vector);
                        break;
                    case 4:
                        oddValues(vector);
                        break;
                    case 5:
                        divisibleForN(vector);
                        break;
                    case 6:
                        averageDeviation(vector);
                        break;
                    case 7:
                        evenPositions(vector);
                        break;
                    case 8:
                        oddPositions(vector);
                        break;
                    case 9:
                        reverseOrder(vector);
                        break;
                    case 10:
                        vector = generateNewVector(vector);
                        break;
                    case 11:
                        vector = introduceNewVector(vector);
                        break;
                    case 12:
                        Environment.Exit(0);
                        break;
                }
            }
        }
        static void visualizeVector(int[] vector)
        {
            Console.WriteLine("\n\n\t\t\tMENU");
            Console.WriteLine("\t\t\t____");
            Console.WriteLine("\n1.\tVisualizar el vector.");
            for (int x = 0; x < vector.Length; x++) Console.Write("\t" + vector[x] + "   ");
            Console.WriteLine("");
        }
        static void modifyVector(int[] vector)
        {
            String will = "si";
            Console.WriteLine("\n\n\t\t\tMENU");
            Console.WriteLine("\t\t\t____");
            Console.WriteLine("\n2.\tModificar valores del vector.");
            while (will.Equals("si", StringComparison.InvariantCultureIgnoreCase))
            {
                Console.Write("\t\tSeleccione la posición que quieres cambiar:_");
                int i = int.Parse(Console.ReadLine());
                while (i < 0 || i > vector.Length - 1)
                {
                    Console.WriteLine("\t\t\tError.");
                    Console.Write("\t\tIntroduzca la posición del valor que desea cambiar:_");
                    i = int.Parse(Console.ReadLine());
                }
                Console.Write("\t\tIntroduzca el nuevo valor:_");
                vector[i] = int.Parse(Console.ReadLine());
                Console.Write("\t¿Desea cambiar algún valor más? (Si/No)_");
                will = System.Console.ReadLine();
                while (!will.Equals("si", StringComparison.InvariantCultureIgnoreCase)
                    && !will.Equals("no", StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.Write("\t\tError. ¿Desea cambiar algún valor más? (Si/No)_");
                    will = System.Console.ReadLine();
                }
            }
        }
        static void evenValues(int[] vector)
        {
            Console.WriteLine("\n\n\t\t\tMENU");
            Console.WriteLine("\t\t\t____");
            Console.WriteLine("\n3.\tVisualizar valores pares.\n");
            for (int i = 0; i < vector.Length; i++) if (vector[i] % 2 == 0) Console.WriteLine("\t" + vector[i] + "   ");
            Console.WriteLine("");
        }
        static void oddValues(int[] vector)
        {
            Console.WriteLine("\n\n\t\t\tMENU");
            Console.WriteLine("\t\t\t____");
            Console.WriteLine("\n4.\tVisualizar valores impares.\n");
            for (int i = 0; i < vector.Length; i++) if (vector[i] % 2 == 1) Console.WriteLine("\t" + vector[i] + "   ");
            Console.WriteLine("");
        }
        static void divisibleForN(int[] vector)
        {
            String will = "si";
            Console.WriteLine("\n\n\t\t\tMENU");
            Console.WriteLine("\t\t\t____");
            Console.WriteLine("\n5.\tVisualizar la suma de los divisibles para n.");
            while (will.Equals("si", StringComparison.InvariantCultureIgnoreCase))
            {
                int a = 0;
                Console.Write("\t\tIntroduzca un número n para continuar:_");
                int n = int.Parse(Console.ReadLine());
                for (int i = 0; i < vector.Length; i++) if (vector[i] % n == 0) a += vector[i];
                Console.WriteLine("\t\tLa suma de todos los números divisibles para " + n + " es " + a);
                Console.Write("\t¿Desea calcular alguna suma más? (Si/No)_");
                will = System.Console.ReadLine();
                while (!will.Equals("si", StringComparison.InvariantCultureIgnoreCase)
                    && !will.Equals("no", StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.Write("\t\tError. ¿Desea calcular alguna suma más? (Si/No)_");
                    will = System.Console.ReadLine();
                }
            }
            Console.WriteLine("");
        }
        static void averageDeviation(int[] vector)
        {
            float m, d, a = 0;
            Console.WriteLine("\n\n\t\t\tMENU");
            Console.WriteLine("\t\t\t____");
            Console.WriteLine("\n6.\tVisualizar la desviación respecto de la media.");
            for (int x = 0; x < vector.Length; x++) a += vector[x];
            m = a / vector.Length;
            Console.WriteLine("\n\t\tLa suma es " + a + " media es " + m);
            for (int i = 0; i < vector.Length; i++)
            {
                d = Math.Abs(vector[i] - m);
                Console.WriteLine("\n\t\t\ti = " + (i + 1) + "\tx = " + vector[i] + "\td = " + d);
            }
            Console.WriteLine("");
        }
        static void evenPositions(int[] vector)
        {
            Console.WriteLine("\n\n\t\t\tMENU");
            Console.WriteLine("\t\t\t____");
            Console.WriteLine("\n7.\tVisualizar posiciones pares.");
            for (int i = 0; i < vector.Length; i++) if (i % 2 == 0) Console.WriteLine("\t" + vector[i] + "   ");
        }
        static void oddPositions(int[] vector)
        {
            Console.WriteLine("\n\n\t\t\tMENU");
            Console.WriteLine("\t\t\t____");
            Console.WriteLine("\n8.\tVisualizar posiciones impares.");
            for (int i = 0; i < vector.Length; i++) if (i % 2 == 1) Console.WriteLine("\t" + vector[i] + "   ");
        }
        static void reverseOrder(int[] vector)
        {
            Console.WriteLine("\n\n\t\t\tMENU");
            Console.WriteLine("\t\t\t____");
            Console.WriteLine("\n9.\tVisualizar los valores en orden inverso.");
            for (int i = vector.Length - 1; i >= 0; i--) Console.WriteLine("\t" + vector[i] + "   ");
        }
        static int[] generateNewVector(int[] vector)
        {
            Random rnd = new Random();
            int j;
            Console.WriteLine("\n\n\t\t\tMENU");
            Console.WriteLine("\t\t\t____");
            Console.WriteLine("\n10.\tGenerar un nuevo vector.");
            Console.Write("\t\t¿Desea concretar la dimensión del nuevo vector? (Si/No)_");
            String will = System.Console.ReadLine();
            while (!will.Equals("no", StringComparison.InvariantCultureIgnoreCase)
                && !will.Equals("si", StringComparison.InvariantCultureIgnoreCase))
            {
                Console.Write("\t\t\tError. ¿Desea concretar la dimensión del nuevo vector? (Si/No)_");
                will = System.Console.ReadLine();
            }
            if (will.Equals("si", StringComparison.InvariantCultureIgnoreCase))
            {
                Console.Write("\t\tIntroduca la dimensión del nuevo vector:_");
                j = int.Parse(Console.ReadLine());
            }
            else j = (int)(rnd.Next(1, 15));
            vector = new int[j];
            for (int k = 0; k < vector.Length; k++) vector[k] = (int)(rnd.Next(0, 30));
            for (int x = 0; x < vector.Length; x++) Console.Write("\t" + vector[x] + "   ");
            return (vector);
        }
        static int[] introduceNewVector(int[] vector)
        {
            String will;
            Console.WriteLine("\n\n\t\t\tMENU");
            Console.WriteLine("\t\t\t____");
            Console.WriteLine("\n11.\tIntroducir un nuevo vector.");
            Console.Write("\t\tIntroduca la dimensión del nuevo vector:_");
            int j = int.Parse(Console.ReadLine());
            vector = new int[j];
            for (int x = 0; x < j; x++)
            {
                Console.Write("\t\tIntroduzca un número en la posición " + x + " del nuevo vector:_");
                int n = int.Parse(Console.ReadLine());
                vector[x] = n;
            }
            do
            {
                Console.WriteLine("\t\tEl nuevo vector es:");
                Console.Write("\t");
                for (int x = 0; x < j; x++) Console.Write("\t" + vector[x] + "   ");
                Console.Write("\n\t\t¿El nuevo vector es correcto? (Si/No)_");
                will = System.Console.ReadLine();
                while (!will.Equals("si", StringComparison.InvariantCultureIgnoreCase)
                    && !will.Equals("no", StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.Write("\t\t\tError. ¿El nuevo vector es correcto? (Si/No)_");
                    will = System.Console.ReadLine();
                }
                if (will.Equals("no", StringComparison.InvariantCultureIgnoreCase))
                {
                    String confirm = "si"; do
                    {
                        Console.Write("\t\t\tIntroduzca la posición del valor que desea cambiar:_");
                        int p = int.Parse(Console.ReadLine());
                        while (p < 0 || p > j - 1)
                        {
                            Console.WriteLine("\t\t\t\tError.");
                            Console.Write("\t\t\tIntroduzca la posición del valor que desea cambiar:_");
                            p = int.Parse(Console.ReadLine());
                        }
                        Console.Write("\t\t\tIntroduzca un nuevo valor para la posición " + p + ":_");
                        int v = int.Parse(Console.ReadLine());
                        vector[p] = v;
                        Console.Write("\t\t\t¿Desea cambiar algún valor más? (Si/No)_");
                        confirm = System.Console.ReadLine();
                        while (!confirm.Equals("si", StringComparison.InvariantCultureIgnoreCase)
                            && !confirm.Equals("no", StringComparison.InvariantCultureIgnoreCase))
                        {
                            Console.Write("\t\t\tError. ¿Desea cambiar algún valor más? (Si/No)_");
                            confirm = System.Console.ReadLine();
                        }
                    } while (confirm.Equals("si", StringComparison.InvariantCultureIgnoreCase));
                }
            } while (will.Equals("no", StringComparison.InvariantCultureIgnoreCase));
            return (vector);
        }
    }
}