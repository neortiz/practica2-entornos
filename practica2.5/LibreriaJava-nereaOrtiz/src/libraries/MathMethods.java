package libraries;

import java.util.Scanner;

public class MathMethods {
	
	static Scanner in = new Scanner(System.in);
	
	public static void calcularHipotenusa(){
		
		double h, a, b;
		
		System.out.println("Introduce los catetos del tri�ngulo.");
		System.out.println("A: ");
		a = Double.parseDouble(in.nextLine());
		System.out.println("B: ");
		b = Double.parseDouble(in.nextLine());
		
		h = Math.sqrt(Math.pow(a, 2)+Math.pow(b, 2));
		
		System.out.println("La hipotenusa mide "+h+" unidades.");
	}
	
	public static void calcularEcuacionSegundoGrado(){
		
		double a, b, c;
		double [] x = new double [2];
		
		System.out.println("Introduce los coeficientes de la ecuaci�n:");
		System.out.println("Coeficiente de x^2:");
		a = Double.parseDouble(in.nextLine());
		System.out.println("Coeficiente de x:");
		b = Double.parseDouble(in.nextLine());
		System.out.println("Coeficiente libre:");
		c = Double.parseDouble(in.nextLine());
		
		System.out.println("La ecuaci�n es "+a+"�x^2+"+b+"�x+"+c);
		
		x[0] = (-b+Math.sqrt(Math.pow(b, 2)-4*a*c))/2*a;
		x[1] = (-b-Math.sqrt(Math.pow(b, 2)-4*a*c))/2*a;
		
		System.out.println("Las soluciones son "+x[0]+" y "+x[1]+".");
	}
	
	public static void logaritmoBaseADeN(){
		
		double a, b, n;
		
		System.out.print("Introduce la base 'a' del logaritmo: ");
		a = Double.parseDouble(in.nextLine());
		System.out.print("Introduce un n�mero para calcular su logaritmo en base "+a+":");
		n = Double.parseDouble(in.nextLine());
		
		b = Math.log(n)/Math.log(a);
		
		System.out.println("El logaritmo en base "+a+" de "+n+" es "+b+".");
	}
	
	public static void puntoMedioSegemento(){
		
		double a1, b1, a2, b2;
		double [] m = new double[2];
		
		System.out.println("Introduce el punto A:");
		System.out.print("a1:");
		a1 = Double.parseDouble(in.nextLine());
		System.out.print("a2:");
		a2 = Double.parseDouble(in.nextLine());
		
		System.out.println("Introduce el punto B:");
		System.out.print("b1:");
		b1 = Double.parseDouble(in.nextLine());
		System.out.print("b2:");
		b2 = Double.parseDouble(in.nextLine());
		
		m[0] = (a1+b1)/2;
		m[1] = (a2+b2)/2;
		
		System.out.println("El punto medio entre el segmento entre A("+a1+", "+a2+") y B("+b1+", "+b2+") es M("+m[0]+", "+m[1]+").");
	}
	
	public static void anguloEntreDosVectores(){
		
		double a, u1, v1, u2, v2, c;
		
		System.out.println("Introduce el vector U:");
		System.out.print("u1:");
		u1 = Double.parseDouble(in.nextLine());
		System.out.print("u2:");
		u2 = Double.parseDouble(in.nextLine());
		
		System.out.println("Introduce el punto V:");
		System.out.print("v1:");
		v1 = Double.parseDouble(in.nextLine());
		System.out.print("v2:");
		v2 = Double.parseDouble(in.nextLine());
		
		c = (u1*v1 + u2*v2)/Math.sqrt(Math.pow(u1, 2)+Math.pow(u2, 2))*Math.sqrt(Math.pow(v1, 2)+Math.pow(v2, 2));
		
		a = Math.toDegrees(Math.acos(Math.toRadians(c)));
		
		System.out.println("El �ngulo entre U=("+u1+", "+u2+") y V=("+v1+", "+v2+") es de "+a+"�.");
	}
	
}
