package libraries;

import java.util.Scanner;

public class StringMethods {
	
	static Scanner in = new Scanner(System.in);
	
	public static void contarPalabras(){
		String cadena;
		
		System.out.print("Introduce una cadena: ");
		cadena = in.nextLine();
		
		System.out.println("Esta cadena tiene "+cadena.split(" ").length+" palabras.");
	}
	
	public static void contarCaracteresPorPalabra(){
		String cadena;
		String [] palabra;
		int i=1;
		
		System.out.print("Introduce una cadena: ");
		cadena = in.nextLine();
		
		palabra=cadena.split(" ");
		
		for (String frase : palabra ){
		    System.out.println("Palabra "+i+": "+String.format("%s", frase));
		    System.out.println("Cantidad de caracteres:"+frase.length());
		    i++;
		}
	}
	
	public static void revertirCadena(){
		
		String cadena;
		
		System.out.print("Introduce una cadena: ");
		cadena = in.nextLine();				
		
		StringBuilder builder = new StringBuilder(cadena);
	    String cadenareverse=builder.reverse().toString();
	    
	    System.out.println("La cadena invertida es: "+cadenareverse);
	}
	
	public static void esPalindromo(){
		
		String cadena;
		
		System.out.print("Introduce una cadena: ");
		cadena = in.nextLine();
		
		String cadena1 = cadena.replace(" ", "");
		
		StringBuilder builder = new StringBuilder(cadena);
	    String cadenareverse=builder.reverse().toString();
	    
	    if(cadena1.equalsIgnoreCase(cadenareverse)) System.out.println("\nLa cadena "+cadena+" es un palindromo");
		else System.out.println("\nLa cadena "+cadena+" no es un palindromo");
	}
	
	public static void contarVocales(){
		String palabra = "";
		int a = 0;
		
		System.out.print("Introduce una cadena: ");
		palabra = in.nextLine();
		
	    for(int x=0;x<palabra.length();x++) {
	        if (palabra.charAt(x)=='a' || palabra.charAt(x)=='e' || palabra.charAt(x)=='i' || palabra.charAt(x)=='o' || palabra.charAt(x)=='u') a++;
	     }
	    
	    System.out.println("Hay "+a+" vocales en esta cadena");
	}
}
