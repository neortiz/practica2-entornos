﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriasVS_nereaOrtiz
    {
        public class MathMethods
        {
            public static void calcularHipotenusa()
            {

                double h, a, b;

                System.Console.WriteLine("Introduce los catetos del triángulo.");
                System.Console.WriteLine("A: ");
                a = double.Parse(Console.ReadLine());
                System.Console.WriteLine("B: ");
                b = double.Parse(Console.ReadLine());

                h = Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));

                System.Console.WriteLine("La hipotenusa mide " + h + " unidades.");
            }

            public static void calcularEcuacionSegundoGrado()
            {

                double a, b, c;
                double[] x = new double[2];

                System.Console.WriteLine("Introduce los coeficientes de la ecuación:");
                System.Console.WriteLine("Coeficiente de x^2:");
                a = double.Parse(Console.ReadLine());
                System.Console.WriteLine("Coeficiente de x:");
                b = double.Parse(Console.ReadLine());
                System.Console.WriteLine("Coeficiente libre:");
                c = double.Parse(Console.ReadLine());

                System.Console.WriteLine("La ecuación es " + a + "·x^2+" + b + "·x+" + c);

                x[0] = (-b + Math.Sqrt(Math.Pow(b, 2) - 4 * a * c)) / 2 * a;
                x[1] = (-b - Math.Sqrt(Math.Pow(b, 2) - 4 * a * c)) / 2 * a;

                System.Console.WriteLine("Las soluciones son " + x[0] + " y " + x[1] + ".");
            }

            public static void LogaritmoBaseADeN()
            {

                double a, b, n;

                System.Console.Write("Introduce la base 'a' del Logaritmo: ");
                a = double.Parse(Console.ReadLine());
                System.Console.Write("Introduce un número para calcular su Logaritmo en base " + a + ":");
                n = double.Parse(Console.ReadLine());

                b = Math.Log(n) / Math.Log(a);

                System.Console.WriteLine("El Logaritmo en base " + a + " de " + n + " es " + b + ".");
            }

            public static void puntoMedioSegemento()
            {

                double a1, b1, a2, b2;
                double[] m = new double[2];

                System.Console.WriteLine("Introduce el punto A:");
                System.Console.Write("a1:");
                a1 = double.Parse(Console.ReadLine());
                System.Console.Write("a2:");
                a2 = double.Parse(Console.ReadLine());

                System.Console.WriteLine("Introduce el punto B:");
                System.Console.Write("b1:");
                b1 = double.Parse(Console.ReadLine());
                System.Console.Write("b2:");
                b2 = double.Parse(Console.ReadLine());

                m[0] = (a1 + b1) / 2;
                m[1] = (a2 + b2) / 2;

                System.Console.WriteLine("El punto medio entre el segmento entre A(" + a1 + ", " + a2 + ") y B(" + b1 + ", " + b2 + ") es M(" + m[0] + ", " + m[1] + ").");
            }

            public static void anguloEntreDosVectores()
            {

                double a, u1, v1, u2, v2, c;

                System.Console.WriteLine("Introduce el vector U:");
                System.Console.Write("u1:");
                u1 = double.Parse(Console.ReadLine());
                System.Console.Write("u2:");
                u2 = double.Parse(Console.ReadLine());

                System.Console.WriteLine("Introduce el punto V:");
                System.Console.Write("v1:");
                v1 = double.Parse(Console.ReadLine());
                System.Console.Write("v2:");
                v2 = double.Parse(Console.ReadLine());

                c = (u1 * v1 + u2 * v2) / Math.Sqrt(Math.Pow(u1, 2) + Math.Pow(u2, 2)) * Math.Sqrt(Math.Pow(v1, 2) + Math.Pow(v2, 2));

                a = RadianToDegree(Math.Acos(DegreeToRadian(c)));

                System.Console.WriteLine("El ángulo entre U=(" + u1 + ", " + u2 + ") y V=(" + v1 + ", " + v2 + ") es de " + a + "º.");
            }
            private static double DegreeToRadian(double angle)
            {
                return Math.PI * angle / 180.0;
            }
            private static double RadianToDegree(double angle)
            {
                return angle * (180.0 / Math.PI);
            }
        }
    }


