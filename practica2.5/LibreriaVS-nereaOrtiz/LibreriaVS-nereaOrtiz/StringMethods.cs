﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriasVS_nereaOrtiz
{
    public class StringMethods
    {
        public static void contarPalabras()
        {
            String cadena;

            System.Console.Write("Introduce una cadena: ");
            cadena = System.Console.ReadLine();

            System.Console.WriteLine("Esta cadena tiene " + cadena.Split(' ').Length + " palabras.");
        }

        public static void contarCaracteresPorPalabra()
        {
            String cadena;
            String[] palabra;

            System.Console.Write("Introduce una cadena: ");
            cadena = System.Console.ReadLine();

            palabra = cadena.Split(' ');

            for (int j = 0; j < palabra.Length; j++)
            {
                System.Console.WriteLine("Palabra " + j + 1 + ": " + palabra[j]);
                System.Console.WriteLine("Cantidad de caracteres:" + palabra[j].Length);
            }
        }

        public static void revertirCadena()
        {

            String cadena;
            String almacen = " ", palindromo = " ";
            int i = 1;

            System.Console.Write("Introduce una cadena: ");
            cadena = System.Console.ReadLine();
            
            do
            {
                almacen = cadena.Substring(cadena.Length - i, 1);
                palindromo = palindromo + almacen;
                i++;
            } while (i <= cadena.Length);

            System.Console.Write(palindromo);

        }

        public static void esPalindromo()
        {

            String cadena;
            String almacen = " ", palindromo = " ";
            int i = 1;

            System.Console.Write("Introduce una cadena: ");
            cadena = System.Console.ReadLine();

            String cadena1 = cadena.Replace(" ", "");

            do
            {
                almacen = cadena.Substring(cadena.Length - i, 1);
                palindromo = palindromo + almacen;
                i++;
            } while (i <= cadena.Length);
            
            palindromo = palindromo.Replace(" ", "");

            if (cadena1.Equals(palindromo, StringComparison.InvariantCultureIgnoreCase)) System.Console.WriteLine("\nLa cadena " + cadena + " es un palindromo");
            else System.Console.WriteLine("\nLa cadena " + cadena + " no es un palindromo");
        }

        public static void contarVocales()
        {
            String palabra = "";
            int a = 0;

            System.Console.Write("Introduce una cadena: ");
            palabra = System.Console.ReadLine();

            for (int x = 0; x < palabra.Length; x++)
            {
                if (palabra[x] == 'a' || palabra[x] == 'e' || palabra[x] == 'i' || palabra[x] == 'o' || palabra[x] == 'u') a++;
            }

            System.Console.WriteLine("Hay " + a + " vocales en esta cadena");
        }
    }
}
