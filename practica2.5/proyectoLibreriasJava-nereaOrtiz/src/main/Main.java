package main;

import java.util.Scanner;

import libraries.MathMethods;
import libraries.StringMethods;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		
		int menu=0;
		
		while(menu>=0){
			System.out.println("\n\t\t\tMENU");
			System.out.println("\t\t\t____");
			System.out.println("\n1.\tCalcular la hipotenusa de un tri�ngulo.");
			System.out.println("2.\tCalcular una ecuaci�n de segundo grado.");
			System.out.println("3.\tCalcular el logaritmo en base a de N.");
			System.out.println("4.\tCalcular el punto medio del segmento AB.");
			System.out.println("5.\tCalcular el angulo entre dos vectores.");
			System.out.println("6.\tContar las palabras de una cadena.");
			System.out.println("7.\tContar los caracteres de cada palabra de una cadena.");
			System.out.println("8.\tMostrar una cadena al rev�s.");
			System.out.println("9.\tMostrar si una cadena es o no un pal�ndromo");
			System.out.println("10.\tContar las vocales de una cadena");		
			System.out.println("11.\tSalir.");
			
			System.out.print("\t\tElegir opci�n:_");
			menu = Integer.parseInt(in.nextLine());
			while(menu<1 || menu>11){
				System.out.print("\t\t\tIntroduzca una opci�n v�lida:_");
				menu = Integer.parseInt(in.nextLine());
			}
			
			switch(menu){
				case 1: 	MathMethods.calcularHipotenusa();;
							break;
				case 2:		MathMethods.calcularEcuacionSegundoGrado();
							break;
				case 3: 	MathMethods.logaritmoBaseADeN();
							break;
				case 4: 	MathMethods.puntoMedioSegemento();
							break;
				case 5:		MathMethods.anguloEntreDosVectores();
							break;
				case 6: 	StringMethods.contarPalabras();
							break;
				case 7: 	StringMethods.contarCaracteresPorPalabra();
							break;
				case 8: 	StringMethods.revertirCadena();
							break;
				case 9: 	StringMethods.esPalindromo();
							break;
				case 10:	StringMethods.contarVocales();
							break;
				case 11:	System.exit(0);
			}
		}
		in.close();
	}

}
