﻿using System;
using LibreriasVS_nereaOrtiz;


namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int menu = 0;

            while (menu >= 0)
            {
                System.Console.WriteLine("\n\t\t\tMENU");
                System.Console.WriteLine("\t\t\t____");
                System.Console.WriteLine("\n1.\tCalcular la hipotenusa de un triángulo.");
                System.Console.WriteLine("2.\tCalcular una ecuación de segundo grado.");
                System.Console.WriteLine("3.\tCalcular el logaritmo en base a de N.");
                System.Console.WriteLine("4.\tCalcular el punto medio del segmento AB.");
                System.Console.WriteLine("5.\tCalcular el angulo entre dos vectores.");
                System.Console.WriteLine("6.\tContar las palabras de una cadena.");
                System.Console.WriteLine("7.\tContar los caracteres de cada palabra de una cadena.");
                System.Console.WriteLine("8.\tMostrar una cadena al revés.");
                System.Console.WriteLine("9.\tMostrar si una cadena es o no un palíndromo");
                System.Console.WriteLine("10.\tContar las vocales de una cadena");
                System.Console.WriteLine("11.\tSalir.");

                System.Console.Write("\t\tElegir opción:_");
                menu = int.Parse(Console.ReadLine());
                while (menu < 1 || menu > 11)
                {
                    System.Console.Write("\t\t\tIntroduzca una opción válida:_");
                    menu = int.Parse(Console.ReadLine());
                }

                switch (menu)
                {
                    case 1:
                        MathMethods.calcularHipotenusa(); 
                        break;
                    case 2:
                        MathMethods.calcularEcuacionSegundoGrado();
                        break;
                    case 3:
                        MathMethods.LogaritmoBaseADeN();
                        break;
                    case 4:
                        MathMethods.puntoMedioSegemento();
                        break;
                    case 5:
                        MathMethods.anguloEntreDosVectores();
                        break;
                    case 6:
                        StringMethods.contarPalabras();
                        break;
                    case 7:
                        StringMethods.contarCaracteresPorPalabra();
                        break;
                    case 8:
                        StringMethods.revertirCadena();
                        break;
                    case 9:
                        StringMethods.esPalindromo();
                        break;
                    case 10:
                        StringMethods.contarVocales();
                        break;
                    case 11:
                        Environment.Exit(0);
                        break;
                }
            }
        }
    }
}